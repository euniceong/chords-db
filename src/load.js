import { generate } from './tools';
import db from './db';
import { MongoClient } from 'mongodb';
import assert from 'assert';

// Connection URL;
const url = process.env.DB_URI;

const CHORD_DB_COLLECTION = process.env.CHORD_DB_COLLECTION || 'chordChart';

const processCommand = () => {
  // Use connect method to connect to the server
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log('Connected successfully to server');

    var database = client.db();


    database.collection(CHORD_DB_COLLECTION).remove({}, function(err,res){
      if (err) throw err;
      var array = [];

      Object.keys(db).map(instrument => {
        var chords = generate(db[instrument]).chords;

        Object.keys(chords).map(key => {
          var chord = chords[key];
          chord.forEach(c => {
            var obj = {};
            obj.instrument = instrument;
            obj.key = c.key;
            obj.suffix = c.suffix;
            obj.positions = c.positions;
            array.push(obj);
          });
        });
      });

      database.collection(CHORD_DB_COLLECTION).insertMany(array, function(err, res) {
        if (err) throw err;
        console.log('documents inserted');
        client.close();
      });
    });
  });
    
};


processCommand();
